FROM ubuntu:18.04
MAINTAINER Wooyoung Kim <wykim@rosettakorea.com>

RUN apt-get update
RUN apt-get install -y nginx nodejs npm git

WORKDIR /opt
RUN git clone git://github.com/mozilla/pdf.js.git

WORKDIR /opt/pdf.js
RUN npm install -g gulp-cli
RUN npm install -g forever
RUN npm install
RUN forever start node_modules/gulp/bin/gulp.js server

ADD sites-available_default /etc/nginx/sites-available/default

RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN chown -R www-data:www-data /var/lib/nginx

VOLUME ["/var/log/nginx"]

WORKDIR /etc/nginx

CMD ["nginx"]

EXPOSE 80